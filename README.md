# Durango beamer theme

This is a work in progress.
If you find it useful use it on your own risk.

## Basic usage

- Add all files to the root folder of your Beamer presentation.
- Add the following line to your main TeX code `\usetheme{Durango}`.
- Change the logo with something else, `img/UPV-EHU.pdf`. 
  If the logo path ins changed, change wherever appears in `beamerinnerthemeDurango.sty`.